# Packet tracer

- `enable` - войти в root
- `conf t` - configure terminal
- `interface <NAME>` - перейти в настройки интерфейса 
- `ip add <ADDR> <MASK>` - назначить айпишник
- `no sh`

- `show ip route` - показать таблицу маршрутизации

- `ip route <dest network> <dest network mask> <next hop address>` - назначение статического маршрута. Нужно прописывать маршруты в обе стороны 
    + `ip route 10.20.20.0 255.255.255.0 20.20.20.2`

- `copy running-config startup-config` (выполнять после enable)

ДЗ: от ПК до сервера,  а обратно идет через верхнюю сеть (верхняя - подробности на фото)