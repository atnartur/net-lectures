# Соединение RIP & OSPF

Поставить один из роутеров и в rip, и в ospf.

rip -> ospf

```
router rip 
redistribute ospf 1 metric 5
exit
```

ospf -> rip 

```
router ospf 1
redistribute rip metric 1000 metric-type 2 subnets 
exit
```

ASBR - autonomous system border router - пограничный роутер автономной системы

ДЗ: http://linkmeup.ru/spsm 0, 1, 3, 6 главы