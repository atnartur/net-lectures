# VLAN

VLAN - деление зон работы коммутатора

По умолчанию существует VLAN 1. Нумеруются от 1-4095.

```
show vlan
```

```
VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Fa0/1, Fa0/2, Fa0/3, Fa0/4
                                                Fa0/5, Fa0/6, Fa0/7, Fa0/8
                                                Fa0/9, Fa0/10, Fa0/11, Fa0/12
                                                Fa0/13, Fa0/14, Fa0/15, Fa0/16
                                                Fa0/17, Fa0/18, Fa0/19, Fa0/20
                                                Fa0/21, Fa0/22, Fa0/23, Fa0/24
                                                Gig0/1, Gig0/2
1002 fddi-default                     active    
1003 token-ring-default               active    
1004 fddinet-default                  active    
1005 trnet-default                    active    

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
1    enet  100001     1500  -      -      -        -    -        0      0
1002 fddi  101002     1500  -      -      -        -    -        0      0   
1003 tr    101003     1500  -      -      -        -    -        0      0   
1004 fdnet 101004     1500  -      -      -        ieee -        0      0   
1005 trnet 101005     1500  -      -      -        ibm  -        0      0   

Remote SPAN VLANs
------------------------------------------------------------------------------

Primary Secondary Type              Ports
------- --------- ----------------- ------------------------------------------
```

1002-1005 - служебные VLAN для совместимости со старым оборудованием (обычно помечаются как `act/unsup`)

## Создание VLAN и назначение имени

```
conf t
vlan 10
name vlan10name
```

## Добавление портов к VLAN

(автоматом создает VLAN, если нет)

```
int range fa0/1-2
switchport mode access
switchport access vlan 10
```

## Показать таблицу MAC адресов

```
show mac address-table
```

Type DYNAMIC - адрес динамически заучен коммутатором

## Соединение узлов в VLAN через несколько коммутаторов

Соединить одинаковые VLAN на двух коммутаторах можно через отдельный кабель.

### TRUNK (tagged) link

Через стандарт **IEEE 802.1Q (dot 1Q)** можно пронести через один кабель несколько VLAN. К Ethernet кадрам перед данными добавляется тег с номером VLAN (VLAN ID - 12 bit) и приоритетю (3 bit).

```
int range fa0/5
switchport trunk encapsulation dot1q - только для eve-ng
switchport mode trunk
```

COS - class of service - есть 8 классов для того, чтобы обеспечивать качество обслуживая (приоретизировать трафик).

native VLAN - не тегированный трафик с VLAN (VLAN 1)

- `show interfaces trunk` - показывает порты в транковом состоянии
- `switchport trunk native vlan 100` - изменить номер native VLAN

VOICE VLAN используется для IP телефонов, видеоконференцсвязи... Канал между IP телефоном и свитчем транковый. 

`switchport voice vlan 30` - добавить voice vlan

# Уровни доступа (пример хорошего дизайна сети)

1. Уровень доступа (access) - хосты, рабочие станции. Используется не особо производительное оборудование
2. Уровень распределения (distribution) - высокопроизводительное оборудование. Обеспечивается полносвязное соединение с уровнем распределения
3. Core уровень - полносвязное подключение к коммутаторам распределения, также есть связь между собой. Нужны для того, чтобы собрать абсолютно все каналы с сети. К этому уровню подключаются серверы (ДЦ), интернет...

Желтый (оранжевый) порт не передает трафик, например, по причине возникновения ошибки и 

# STP (L2) 

Избегает широковещательный шторм.

Bridge ID - (bridge = switch в STP). Состоит из Bridge Priority (0 - 65535, изменяется с шагом в 4096, чтобы VLAN не перемешивались между собой), MAC коммутатора. 32678 приоритет по умолчанию.


1. Выбор root bridge по минимальному bridge id
2. Выбор ролей портов
    - root port - наилучший путь к рутовому свитчу
    - designated port - назначенный порт
3. Где находятся 2 designated порта, вырубается тот, у которго BID больше. Он становится blocked.

STP работает отдельно для каждого VLAN.

- `show spanning-tree` - посмотреть STP 
- `spanning-tree vlan 1-4094 pri 8192` - изменение приоритета

ECMP - equal cost multipassing - многопутевая схема одинаковой ценой (балансировка трафика)

Чтобы использовать все порты и оставить рабочим STP, можно изменить рутовый свитч для одного из вланов.

Состояния порта во время начала работы: BLOCK -> LISTEN -> LEARNING -> FORWARDNING (все это происходит за 50 сек)

## RSTP

Rapid STP - работает быстрее

Переход к RSP: `spanning-tree mode rapid-pvst`

PVST - per VLAN spanning tree

MSTP - работает на группе VLAN

# LACP

Агрегирование каналов (англ. link aggregation) — технологии объединения нескольких параллельных каналов передачи данных в сетях Ethernet в один логический, позволяющие увеличить пропускную способность и повысить надёжность. В различных конкретных реализациях агрегирования используются альтернативные наименования: транкинг портов (англ. port trunking), связывание каналов (link bundling), склейка адаптеров (NIC bonding), сопряжение адаптеров (NIC teaming).

LACP (англ. link aggregation control protocol) — открытый стандартный протокол агрегирования каналов, описанный в документах IEEE 802.3ad и IEEE 802.1aq. Многие производители для своих продуктов используют не стандарт, а патентованные или закрытые технологии, например, Cisco применяет технологию EtherChannel (разработанную в начале 1990-х годов компанией Kalpana[en]), а также нестандартный протокол PAgP.

channel group 1 mode ? active/passive(LACP) // нужен active
desirable/auto PAgP поддерживается только Cisco
Режим On

Создаем виртаульно агрегированный интефейс

```
int range e0/1-2
interface port-channel 1
channel-group 3 mode on
switchport trunk encapsul dot1q // Command rejected: Po1 not a switching port.
swithcport mode trunk
```

```
show brief
sh spaning-tree 
show interface trunk
```

слева руут для в2 а правый для влан 3

# Резервирование default gateway для шлюза

FHRP - first hop redudency protocol (отказоустойчивость первого хопа). Семейство протоколов:
- HSRP - hot standby RP
- VPRP - virtual router RP
- GLBP - gateway load balancing protocol
- GARP - gateway address resolution protocol

Virtual IP - принадлежит двум устройствам. 
- VRIP priority 0...255 - приоритет устройств внутри виртуального IP. Больше - лучше.
- Default priority - 100

```
ip routing 
int vlan 200
ip add 10.0.0.252 255.255.255.0
no sh
vrrp 100 ip <ip address> 
vrrp 100 priority 
```

Задача на "подумать": Все хосты VLAN 100. 2 устройства на core. Хочется, чтобы оба устройства обрабатывали трафик. Используется VRRP, который работает по схеме active/standby. Надо сделать так, чтобы трафик ходил через оба устройства сразу.

Подсказка: мы зачем-то указывали номер группы, когда конфигурировали

+балл у меня

# NAT

Транслирует локальные адреса в публичные

- inside local - адрес во внутренней сети, который мы транслируем
- inside global - тот белый адрес, который относится к маршрутизатору
- outside local & global - адрес сети, который мы пытаемся достичь из внутренней сети

1. Определяем роли: 

```
int e0/0
ip nat inside

int e0/1
ip nat outside
```

2. Включение NAT

```
(config)# ip nat inside source static <inside local> <inside global>
```

## Port address translation (Dynamic NAT)

Позволяет во внутренней сети поддерживать работу нескольких устройств.

Соответствие устанавливается по внутреннему айпишнику и порту источника и назначения

access lists содержат сети с внутренними адресами:

```
no ip nat inside source static
access-list 1 permit 192.168.0.0 0.0.0.255
ip nat inside source list 1 interface e0/1 overload
```

+балл у меня

# VPN - Virtual private network

Наложенная (overlay) виртуальная логическая инфраструктура (сеть) поверх существующей сети.

Underlay - опорная, настоящая, физическая инфраструктура

VPN != безопасность. Это просто добавление заголовков. Чтобы VPN был безопасным, надо делать дополнительные настройки

## GRE - generic router encapsulation 

Общая инкапсуляция маршрутизатора. 

В начало заголовка IP добавляются физические адреса, чтобы пронести данные внутри тунеля.

**Итоговый вид пакета: Eth | IP | GRE | IP | TCP/UDP | Data**

- Первый IP - адреса underlay
- Второй IP - адреса overlay

## Создаем GRE

Нужно прописать на обоих сторонeах, меня IP

```
int tunnel 1 
ip addr 10.100.0.1 255.255.255.0
tunnel source e0/0
tunnel destination 4.4.4.2
tunnel mode gre  ip
```

Маршрутизация:

```
ip route 10.0.1.0 255.255.255.0 tunnel 1
```

Маршрут по умолчанию:

```
ip route 0.0.0.0 0.0.0.0. 1.1.1.2
```

## Добавляем OSPF для резервирования канала

```
router ospf 1
network 10.0.0.0 0.0.0.255 area 0
network 10.100.0.0 0.0.0.255 area 0
network 10.200.0.0 0.0.0.255 area 0
```

Выключаем статический маршрут для того, чтобы все работало только через OSPF:

```
no ip route 10.0.1.0 255.255.255.0 tunnel 1  
```

# IPSec:
1. Обеспечивает конфиденциальность (симметричные алгоритмы шифрования).
2. Обеспечивает целостность (хэширование).
3. Обеспечивает аутентификацию (Pre-Shared Key, Цифровые сертификаты).

------------------------------------------------------------------------------

![](https://gitlab.com/atnartur/net-lectures/raw/master/%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D0%BA%D0%B0/2018-04-02%20IPSec.jpg)

Для начала пропишем маршруты по умолчанию:
- R1: `ip route 0.0.0.0 0.0.0.0 1.1.1.2`
- R3: `ip route 0.0.0.0 0.0.0.0 2.2.2.2`

Для роутера R1

1. Настройка фазы 1

```
(config)#crypto isakmp policy 1
(config-isakmp)#encryption AES
(config-isakmp)#hash sha512
(config-isakmp)#group 20
(config-isakmp)#authentication pre-share
```

2. Определение партнера

```
(config)#crypto isakmp key 123 address 2.2.2.1
```

3. Определение интересного трафика

```
(config)#ip access-list extended List1
(config-list)#permit ip 10.0.0.0 0.0.0.255 10.0.1.0 0.0.0.255
```

4. Фаза 2

```
(config)#crypto ipsec transform-set TSET esp-aes esp-sha512-hmac
```

5. Создание криптокарты

```
crypto map CMAP 1 ipsec-isakmp
match address List1
set peer 2.2.2.1
set transform-set TSET
exit
```
6.
```
int e0/1
crypto map CMAP
```
--------------------------------------------------------------------------

Для роутера R3

1. Настройка фазы 1

```
(config)#crypto isakmp policy 1
(config-isakmp)#encryption AES
(config-isakmp)#hash sha512
(config-isakmp)#group 20
(config-isakmp)#authentication pre-share
```

2. Определение партнера

```
(config)#crypto isakmp key 123 address 1.1.1.1
```

3. Определение интересного трафика

```
(config)#ip access-list extended List1
(config-list)#permit ip 10.0.1.0 0.0.0.255 10.0.0.0 0.0.0.255
```

4. Фаза 2

```
(config)#crypto ipsec transform-set TSET esp-aes esp-sha512-hmac
```

5. Создание криптокарты

```
crypto map CMAP 1 ipsec-isakmp
match address List1
set peer 1.1.1.1
set transform-set TSET
exit
```

6.

```
int e0/1
crypto map CMAP
```

_Информацию предоставил Динар Ахметзянов_

# Gateway Protocols

- IGP - interior gateway protocol
- EGP - exterior gateway protocol

Автономные системы нумеруются. Есть частные номера автономных систем, которые глобально не машрутизируются (64512-65534). Номера автономных систем 32 битные

BGP - border gateway protocol, EGP протокол.

- Single-Homed - одно подключение от дома до провайдера.
- Multi-Homed - два подключения в разные "дома", причем дома тоже соединены между собой.

BGP нужен, если нужно использовать сразу 2 провайдера

- PI - provider independent IP
- PA - provider agregatable

Для PI анонсируются /25 сетки, чтобы не перегружать маршрутизаторы

# BGP

## Поиск оптимального маршрута

Метрика - минимальное колво автономных систем, которые можно пройти до этого маршрута (AS path). Если метрика одинаковая, будет выбираться маршрут, который придет раньше. 

Чтобы трафик не зацикливался, есть фильтр на наличие дублирующихся элементов в пути.

Чтобы установить соседство, нужно установить TCP сессию и явно указать соседа. Если у соседа мы тоже прописаны, то начнется обмен маршрутной информацией.

## Настройка

1. `router bgp 64500` Включение протокола с номером автономной системы
2. `network 100.0.0.0 mask 255.255.254.0` объявляем провайдеронезависимый префикс. Сети на интерфейсах (транзитные) объявлять не нужно
3. `neighbor 101.0.0.1 remote-as 64501` устанавливаем соседа через IP адрес соседнего устройства с указанием номера автономной системы
4. `neighbor 102.0.0.1 remote-as 64502` устанавливаем соседа через IP адрес соседнего устройства с указанием номера автономной системыn

- `show ip bgp neighbors` - показать соседей
- `show ip bgp` - показать префиксы
- `show ip route` - под буквой B будут BGP префиксы

Административное растояние BGP - 20n

`ip route 100.0.0.0 255.255.254.0 null0` route blackholing. Нужно для избежания петель, чтобы пакеты, для которых нет точного маршрута, не болтались

## Получаем default-таблицу от 64502

R4(AS-64502):

```
router bgp 64502
neighbor 102.0.0.2 default-originate
```

## Настраиваем Local Preference

R3(64500):

```
ip prefix-list TEST1_IN seq 5 permit 120.0.0.0/24
ip prefix-list TEST2_IN seq 5 permit 103.0.0.0/20

route-map BGP1_IN permit 10 
match ip address prefix-list TEST1_IN
set local-preference 50

route-map BGP1_IN permit 20
set local-preference 100

route-map BGP2_IN permit 10 
match ip address prefix-list TEST2_IN
set local-preference 50 

route-map BGP2_IN permit 20
set local-preference 100

router bgp 64500
neighbor 101.0.0.1 route-map BGP2_IN in
neighbor 102.0.0.1 route-map BGP1_IN in
```

Проверяем через show ip route в R3(64500) и смотрим, идут ли сети 103.0.0.0/20, 120.0.0.0/24 с разных интерфейсов

## Настраиваем AS_Path

R3(64500):

```
router bgp 64500

route-map AS_PATH_PREP permit 10
set as-path prepend 64500 64500

neighbor 101.0.0.1 route-map AS_PATH_PREP out
```

## Разбиваем AS-64500 на 2 подсети

R3(64500):

```
router bgp 64500
network 100.0.0.0 mask 255.255.254.0
network 100.0.0.0 mask 255.255.255.0
network 100.0.1.0 mask 255.255.255.0

ip route 100.0.0.0 255.255.254.0 Null0
ip route 100.0.0.0 255.255.255.0 Null0
ip route 100.0.1.0 255.255.255.0 Null0

ip prefix-list LIST_OUT1 seq 5 permit 100.0.0.0/24
ip prefix-list LIST_OUT1 seq 10 permit 100.0.0.0/23
ip prefix-list LIST_OUT2 seq 5 permit 100.0.1.0/24
ip prefix-list LIST_OUT2 seq 10 permit 100.0.0.0/23

router bgp 64500
neighbor 101.0.0.1 remote-as 64501
neighbor 101.0.0.1 prefix-list LIST_OUT1 out
neighbor 102.0.0.1 remote-as 64502
neighbor 102.0.0.1 prefix-list LIST_OUT2 out
```


# IBGP

ASPATH всегда одинаковый.

## Предотвращение петли внутри автономки 

Указываем neighbor'ов по принципу "каждый с каждым" (full mesh). Убираем петли руками.

В качестве адреса соседа указывается loopback, так как они не зависят от физических интерфейсов.

Надо сделать так, чтобы про лупбеки все узнали. Запускаем OSPF с указанием кроме directly connected, еще и лупбеки.

В iBGP нужно все равно указывать соседа, даже если нет прямого соединения

_все команды для R1 (который у меня R4)_

## Настройка eBGP

```
router bgp 64500
network 100.0.0.0 mask 255.255.254.0
neighbor 101.0.0.2 remote-as 64501

ip route 100.0.0.0 255.255.254.0 null0
```

## Настройка iBGP
 
```
router bgp 64500
neighbor 4.4.4.4 remote-as 64500
neighbor 4.4.4.4 update-source lo
neighbor 3.3.3.3 remote-as 64500
neighbor 3.3.3.3 update-source lo0
neighbor 2.2.2.2 remote-as 64500
neighbor 2.2.2.2 update-source lo0
```

## OSPF

```
router ospf 1
network 10.0.14.0 0.0.0.255 area 0
network 10.0.12.0 0.0.0.255 area 0
network 1.1.1.1 0.0.0.255 area 0
```

В результате по eBGP трафик ходить будет, а eBGP-iBGP-eBGP трафик ходить пока не будет.

**Split horizon** - префикс, который пришел от iBGP соседа, другому iBGP соседу не передаем. С eBGP все наоборот. Такое поведение позволяет избежать петель. 

**igp sync** - ibgp маршруты редистрибутируются в ospf. Но это плохо, потому что там слишком много информации, и ospf положит маршрутизаторы.

**next hop self**

```
neighbor 1.1.1.1 next-hop-self
neighbor 2.2.2.2 next-hop-self
neighbor 3.3.3.3 next-hop-self
neighbor 4.4.4.4 next-hop-self
```

## Route reflector

перебрасывает маршруты на всех своих соседей. Может быть запасной RR. RR вместе с клиентами называется кластер

На RR:

```
neighbor 2.2.2.2 route-reflector-client
neighbor 3.3.3.3 route-reflector-client
neighbor 4.4.4.4 route-reflector-client
```

# MPLS

Multiprotocol label switching

Label - 20 битный адрес канала. Маршрутизаторы, которые работают в MPLS, маршрутизируют по метке MPLS. 

Ingress LSR (label switch router) - пограничный роутер. У него есть и IP, и MPLS табл маршрутизации. Для каждого маршрута вставляет дополнительно его MPLS header.

Внутри сети MPLS метки изменяются с внутренними номерами. Только пограничные транслируют IP->MPLS и обратно.

Это нужно для того, чтобы внутренние роутеры не напрягались над анализом огромных таблиц IP маршрутизации. Все сложные операции с IP таблицами происходят на пограничных маршрутизаторах.


`mpls ip` - глобальное включение на роутере

включение на интерфейсах:

```
int e0/0
mpls ip
```

`show mpls forwarding-table` - показать таблицу mpls

# VRF - Virtual Routing and Forwarding (Сетевая виртуализация)

VRF - virtual routing forwarder. Помогает распилить маршрутизатор на несколько виртуальных маршрутизаторов, которые будут работать для отдельных клиентов. Каждому вирт маршрутизатору соответствуют 1 или несколько физических интерфейсов.

```
ip vrf cus1
ip vrf cus2

int e0/1
ip vrf forwarding cus1
ip addr 120.0.0.1 
no sh
```

`show ip route vrf cus1`

# MPLS L3 VPN

В MPLS появляется 2 метки. Первая - метка MPLS, вторая - метка клиента. После разбора второй метки трафик будет перенаправляться на VRF нужного клиента.

С точки зрения клиента облако MPLS  выглядит как один маршрутизатор провайдера

Есть несколько типов машрутизатора: 
- P - provider (MPLS)
- PE - provider-edge - на границе с клиентом
- CE - customer-edge - граничный у клиента

## MP BGP 

Протокол маршрутизаторы, передающий VPN метку

```
ip vrf cus1
rd 64500:100
route-target export 64500:100
route-target import 64500:100
```

rd - router destinctor - номер роутера. Далее идет номер автономный системы и номер конфедерации, который нужен для разграничения трафика между клиентами

route target - настройки передачи маршрутов

на 7.7.7.7:

```
router bgp 64500
neighbor 8.8.8.8 remote-as 64500
 neighbor 8.8.8.8 update-source Loopback0 
```

Это буunдет работать, так как iBGP работает через MPLS, и полная связность не нужна 

```
 address-family vpnv4 
 neighbor 8.8.8.8 activate 
 neighbor 8.8.8.8 send-community both 
 exit-address-family 
 
address-family ipv4 vrf cus1 
 redistribute connected 
 redistribute ospf 1000 
 exit-address-family 
 ! 
 address-family ipv4 vrf cus2 
 redistribute connected 
 redistribute ospf 2000 
 exit-address-family 
```

```
router ospf 1000 vrf cus1 
 redistribute bgp 64500 subnets 
 network 120.0.0.0 0.0.0.255 area 0 
 network 121.0.0.0 0.0.0.255 area 0 
! 
router ospf 2000 vrf cus2 
 redistribute bgp 64500 subnets 
 network 120.0.0.0 0.0.0.255 area 0 
 network 121.0.0.0 0.0.0.255 area 0
```


