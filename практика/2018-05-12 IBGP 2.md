**Split horizon** - префикс, который пришел от iBGP соседа, другому iBGP соседу не передаем. С eBGP все наоборот. Такое поведение позволяет избежать петель. 

**igp sync** - ibgp маршруты редистрибутируются в ospf. Но это плохо, потому что там слишком много информации, и ospf положит маршрутизаторы.

**next hop self**

```
neighbor 1.1.1.1 next-hop-self
neighbor 2.2.2.2 next-hop-self
neighbor 3.3.3.3 next-hop-self
neighbor 4.4.4.4 next-hop-self
```

## Route reflector

перебрасывает маршруты на всех своих соседей. Может быть запасной RR. RR вместе с клиентами называется кластер

На RR:

```
neighbor 2.2.2.2 route-reflector-client
neighbor 3.3.3.3 route-reflector-client
neighbor 4.4.4.4 route-reflector-client
```
