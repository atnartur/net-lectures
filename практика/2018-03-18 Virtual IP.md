# Резервирование default gateway для шлюза

FHRP - first hop redudency protocol (отказоустойчивость первого хопа). Семейство протоколов:
- HSRP - hot standby RP
- VPRP - virtual router RP
- GLBP - gateway load balancing protocol
- GARP - gateway address resolution protocol

Virtual IP - принадлежит двум устройствам. 
- VRIP priority 0...255 - приоритет устройств внутри виртуального IP. Больше - лучше.
- Default priority - 100

```
ip routing 
int vlan 200
ip add 10.0.0.252 255.255.255.0
no sh
vrrp 100 ip <ip address> 
vrrp 100 priority 
```

Задача на "подумать": Все хосты VLAN 100. 2 устройства на core. Хочется, чтобы оба устройства обрабатывали трафик. Используется VRRP, который работает по схеме active/standby. Надо сделать так, чтобы трафик ходил через оба устройства сразу.

Подсказка: мы зачем-то указывали номер группы, когда конфигурировали

+балл у меня