# VLAN

VLAN - деление зон работы коммутатора

По умолчанию существует VLAN 1. Нумеруются от 1-4095.

```
show vlan
```

```
VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Fa0/1, Fa0/2, Fa0/3, Fa0/4
                                                Fa0/5, Fa0/6, Fa0/7, Fa0/8
                                                Fa0/9, Fa0/10, Fa0/11, Fa0/12
                                                Fa0/13, Fa0/14, Fa0/15, Fa0/16
                                                Fa0/17, Fa0/18, Fa0/19, Fa0/20
                                                Fa0/21, Fa0/22, Fa0/23, Fa0/24
                                                Gig0/1, Gig0/2
1002 fddi-default                     active    
1003 token-ring-default               active    
1004 fddinet-default                  active    
1005 trnet-default                    active    

VLAN Type  SAID       MTU   Parent RingNo BridgeNo Stp  BrdgMode Trans1 Trans2
---- ----- ---------- ----- ------ ------ -------- ---- -------- ------ ------
1    enet  100001     1500  -      -      -        -    -        0      0
1002 fddi  101002     1500  -      -      -        -    -        0      0   
1003 tr    101003     1500  -      -      -        -    -        0      0   
1004 fdnet 101004     1500  -      -      -        ieee -        0      0   
1005 trnet 101005     1500  -      -      -        ibm  -        0      0   

Remote SPAN VLANs
------------------------------------------------------------------------------

Primary Secondary Type              Ports
------- --------- ----------------- ------------------------------------------
```

1002-1005 - служебные VLAN для совместимости со старым оборудованием (обычно помечаются как `act/unsup`)

## Создание VLAN и назначение имени

```
conf t
vlan 10
name vlan10name
```

## Добавление портов к VLAN

(автоматом создает VLAN, если нет)

```
int range fa0/1-2
switchport mode access
switchport access vlan 10
```

## Показать таблицу MAC адресов

```
show mac address-table
```

Type DYNAMIC - адрес динамически заучен коммутатором

## Соединение узлов в VLAN через несколько коммутаторов

Соединить одинаковые VLAN на двух коммутаторах можно через отдельный кабель.

### TRUNK (tagged) link

Через стандарт **IEEE 802.1Q (dot 1Q)** можно пронести через один кабель несколько VLAN. К Ethernet кадрам перед данными добавляется тег с номером VLAN (VLAN ID - 12 bit) и приоритетю (3 bit).

```
int range fa0/5
switchport trunk encapsulation dot1q - только для eve-ng
switchport mode trunk
```

COS - class of service - есть 8 классов для того, чтобы обеспечивать качество обслуживая (приоретизировать трафик).

native VLAN - не тегированный трафик с VLAN (VLAN 1)

- `show interfaces trunk` - показывает порты в транковом состоянии
- `switchport trunk native vlan 100` - изменить номер native VLAN

VOICE VLAN используется для IP телефонов, видеоконференцсвязи... Канал между IP телефоном и свитчем транковый. 

`switchport voice vlan 30` - добавить voice vlan
